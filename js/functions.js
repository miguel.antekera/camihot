$(document).ready(function() {
 

    // Ease scroll 
    $('.ease').on('click', function(e) {
        e.preventDefault();

        var target = this.hash;
        var $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function() {
            // window.location.hash = target;
        });

        $('.navbar-toggle').click();

        return false;
    });

$( ".trigger-modal" ).each(function(index) {
    $(this).on("click", function(){
    var productName = $(this).attr("data-content");
    console.log(productName);
    $('.modal-content').find('.' + productName).show().siblings('.modal-body').hide();
    });
});


$('.play-video').on('click', function() {
    $('.myvideo').html('<iframe src="https://player.vimeo.com/video/144827889?autoplay=1&rel=0&showinfo=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
    $('.myvideo').addClass('outline');
    return false;
    });


});
